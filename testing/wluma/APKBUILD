# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=wluma
pkgver=4.1.0
pkgrel=1
pkgdesc="Automatic brightness adjustment based on screen contents and ALS"
url="https://github.com/maximbaz/wluma"
# riscv64, s390x: blocked by rust/cargo
# arm*, x86: fails to build due to crappy v4l crate
arch="aarch64 ppc64le x86_64"
license="ISC"
makedepends="
	cargo
	cargo-patch
	clang
	eudev-dev
	v4l-utils-dev
	wayland-dev
	vulkan-loader-dev
	"
install="$pkgname.post-install"
source="https://github.com/maximbaz/wluma/archive/$pkgver/wluma-$pkgver.tar.gz
	cargo.patch
	use-linked-vulkan.patch

	v4l-01-fix-string-pointer-cast.cargo-patch
	v4l-02-fix-wrong-_IOC_TYPE-on-musl.cargo-patch
	"

# wayland-client/use_system_lib - link to libwayland-client.so instead of using Rust impl.
_cargo_opts="--frozen --features wayland-client/use_system_lib"

prepare() {
	default_prepare

	# Optimize binary for size.
	cat >> Cargo.toml <<-EOF

		[profile.release]
		codegen-units = 1
		lto = true
		opt-level = "s"
		panic = "abort"
	EOF

	cargo patch
	cargo fetch --locked
}

build() {
	cargo build $_cargo_opts --release
}

check() {
	cargo test $_cargo_opts
}

package() {
	install -D -m755 target/release/wluma -t "$pkgdir"/usr/bin/
	install -D -m644 90-wluma-backlight.rules -t "$pkgdir"/lib/udev/rules.d/
}

sha512sums="
c677cb4c8974be3bab3ac88c0f3e37dc74c93190a09c76ad97f064e8d845cf5949dc58587bb64804fda6f30b31976072ecc18ceb60a488edd1e7cb58cad1d0e6  wluma-4.1.0.tar.gz
3d1e36938d93af5db0843a41d371bda1cbdf93474eef5ba6ec1fdd72fb476b7f0b7e46bbcc642d7c84876e42efc4050dcb889efa27b229d9385f0b59708a92c0  cargo.patch
ab252c1952567c70b3b2091933332769176cd1789d82bdd0a5686e0f4b4345d8c35cde2adfab5208c7a287d7a0666562878f91779d56f753860f846a1fc2da6c  use-linked-vulkan.patch
cd9e718a2950b71039e2be15c004f5236667fab2025766759e56f46454e24d845b863d400f8655bd2a40e1a4b2366307e263eff714db85062dab8c68df10a838  v4l-01-fix-string-pointer-cast.cargo-patch
b19d53e00859005d2a91cc6d0d99450e753bbce3b5843e26d0153ddbe38835346dea580988573bd1c7230effe5a260889bc3b82b71dfba2a6d9106253f62dc0e  v4l-02-fix-wrong-_IOC_TYPE-on-musl.cargo-patch
"
