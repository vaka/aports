# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=syft
pkgver=0.37.7
pkgrel=0
pkgdesc="Generate a Software Bill of Materials (SBOM) from container images and filesystems"
url="https://github.com/anchore/syft"
license="Apache-2.0"
arch="all !armhf !armv7 !x86" # FTBFS on 32-bit arches
makedepends="go"
source="https://github.com/anchore/syft/archive/v$pkgver/syft-$pkgver.tar.gz"
options="!check" # tests need docker

export GOFLAGS="$GOFLAGS -trimpath -mod=readonly -modcacherw"
export GOPATH="$srcdir"
export CGO_ENABLED=0

build() {
	go build -o bin/syft
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/syft "$pkgdir"/usr/bin/syft
}

sha512sums="
391f5e6c4a794ba7ea3024c9ef80e4f3a5706a106189598a5ac5c30148cc0eb0b677d019efd637b7fa0fafcf82f7def523eeea2863537266cf59f5997a4568c6  syft-0.37.7.tar.gz
"
