# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-isort
_pkgname=isort
pkgver=5.9.3
_tag=${pkgver}
pkgrel=0
pkgdesc="Python library to sort Python imports"
url="https://github.com/timothycrosley/isort"
arch="noarch"
license="MIT"
# 3 out of 111 tests are failing. Needs upstream fix.
options="!check"
depends="python3"
makedepends="py3-pip"
checkdepends="py3-pytest"
source="$pkgname-$_tag.tar.gz::https://github.com/timothycrosley/isort/archive/$_tag.tar.gz"
builddir="$srcdir"/$_pkgname-$_tag

replaces="py-isort" # Backwards compatibility
provides="py-isort=$pkgver-r$pkgrel" # Backwards compatibility

check() {
	pytest-3 test_isort.py
}

package() {
	python3 -m pip install . --prefix=/usr --root="$pkgdir"
}

sha512sums="96aa9c8bb58c95a41f562d3f0e1cf6e9c0c4158486bca24dfe2293fc86c7c6b74e3e6a84ed755d965b692b1f06d98cdc8ed69080775f8acd9186ac3ae6401570  py3-isort-5.9.3.tar.gz"
